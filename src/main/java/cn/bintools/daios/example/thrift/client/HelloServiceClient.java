package cn.bintools.daios.example.thrift.client;

import cn.bintools.daios.example.thrift.HelloService;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

/**
 * 客户端
 *
 * @author <a href="jian.huang@bintools.cn">yunzhe</a>
 * @version 1.0.0 2019-07-02-下午7:15
 */
public class HelloServiceClient {
    public static void main(String[] args) {
        try {
            TTransport tTransport = new TSocket("127.0.0.1", 9899);
            tTransport.open();

            TProtocol protocol = new TBinaryProtocol(tTransport);
            HelloService.Client client = new HelloService.Client(protocol);
            System.out.println(client.add(200,200));
            System.out.println("连接名称"+client.getConnInfoById(12));
            tTransport.close();
        } catch (TTransportException e) {
            e.printStackTrace();
        }catch (TException e){
            e.fillInStackTrace();
        }

    }
}    