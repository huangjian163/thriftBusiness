package cn.bintools.daios.example.thrift.config;

import cn.bintools.daios.example.thrift.listener.ThriftServerStartListener;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * config 注册Thrift监听器
 *
 * @author <a href="jian.huang@bintools.cn">yunzhe</a>
 * @version 1.0.0 2019-07-02-下午8:11
 */
@Configuration
public class ThrfitConfig {

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Bean
    public ServletListenerRegistrationBean listenerRegist() {
        ServletListenerRegistrationBean srb = new ServletListenerRegistrationBean();
        srb.setListener(new ThriftServerStartListener());
        return srb;
    }
}