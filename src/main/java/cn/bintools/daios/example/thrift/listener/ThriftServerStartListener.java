package cn.bintools.daios.example.thrift.listener;

import cn.bintools.daios.example.thrift.server.ThriftServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * thrift服务启动监听器
 *
 * @author <a href="jian.huang@bintools.cn">yunzhe</a>
 * @version 1.0.0 2019-06-20-下午4:36
 */
@Slf4j
public class ThriftServerStartListener implements ServletContextListener {
    private static ThriftServer thriftServer;
    @Override
    public void contextInitialized(ServletContextEvent event) {
        try {
            ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());
            thriftServer = context.getBean(ThriftServer.class);
            thriftServer.start();
        }catch (Exception e){
            log.error("开始thrift异常");
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}