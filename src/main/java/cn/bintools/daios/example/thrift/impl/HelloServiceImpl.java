package cn.bintools.daios.example.thrift.impl;

import cn.bintools.daios.example.thrift.ConnectionInfo;
import cn.bintools.daios.example.thrift.HelloService;
import cn.bintools.daios.example.thrift.server.BusinessServer;
import org.apache.thrift.TException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.stereotype.Service;

/**
 * 服务端实现
 *
 * @author <a href="jian.huang@bintools.cn">yunzhe</a>
 * @version 1.0.0 2019-07-02-下午7:14
 */
@Service
public class HelloServiceImpl implements HelloService.Iface,ApplicationListener<ApplicationContextEvent> {
    private static ApplicationContext applicationContext;
    @Override
    public int add(int num1, int num2) throws TException {
        BusinessServer server = applicationContext.getBean(BusinessServer.class);
        return server.business();
    }

    @Override
    public ConnectionInfo getConnInfoById(int cpId) throws TException {
        ConnectionInfo connectionInfo = new ConnectionInfo();
        connectionInfo.setConnId(12);
        connectionInfo.setConnectionName("ThrfaceiftConnName");
        connectionInfo.setUrl("192.168.1.162");
        connectionInfo.setPort(3306);
        connectionInfo.setUserName("mysql_conn");
        connectionInfo.setPassword("abc*&ABC123");
        return connectionInfo;
    }

    @Override
    public void onApplicationEvent(ApplicationContextEvent event) {
        applicationContext = event.getApplicationContext();
    }
}