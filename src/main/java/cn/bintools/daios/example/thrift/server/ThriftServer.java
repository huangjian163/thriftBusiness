package cn.bintools.daios.example.thrift.server;

import cn.bintools.daios.example.thrift.HelloService;
import cn.bintools.daios.example.thrift.impl.HelloServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransportException;
import org.apache.thrift.transport.TTransportFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 初始化thrift服务
 *
 * @author <a href="jian.huang@bintools.cn">yunzhe</a>
 * @version 1.0.0 2019-07-02-下午7:10
 */
@Component
@Slf4j
public class ThriftServer implements ApplicationContextAware,InitializingBean {
    @Value("${thrift.port}")
    private int port;

    private TBinaryProtocol.Factory protocolFactory;

    private TTransportFactory transportFactory;

    private static ApplicationContext context;



    public void init(){
        protocolFactory = new TBinaryProtocol.Factory();
        transportFactory = new TTransportFactory();
    }

    public void start(){
        new Thread(){
            @Override
            public void run(){
                TProcessor processor = new HelloService.Processor<HelloService.Iface>(new HelloServiceImpl());
                init();
                try{
                    TServerSocket serverSocket = new TServerSocket(port);
                    TThreadPoolServer.Args args = new TThreadPoolServer.Args(serverSocket);
                    args.protocolFactory(protocolFactory);
                    args.processor(processor);
                    args.transportFactory(transportFactory);
                    TServer server = new TThreadPoolServer(args);
                    log.info("thrift server start success, port={}",port);
                    server.serve();
                }catch (TTransportException e){
                    log.error("thrift server start fail",e);
                }
            }
        }.start();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context=applicationContext;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }
}