package cn.bintools.daios.example.thrift.server;

import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author <a href="jian.huang@bintools.cn">yunzhe</a>
 * @version 1.0.0 2019-07-03-下午7:29
 */
@Service
public class BusinessServer {

    public int business(){
        return 10000;
    }
}    